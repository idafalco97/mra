package tdd.training.mra;

import java.util.*;
import java.util.regex.Pattern;

public class MarsRover {
	protected int planetX;
	protected int roverX;
	protected int planetY; 
	protected int roverY;
	protected List<String> planetObstacles;
	protected Set<String> obstacles;
	protected Direction direction;
	protected String status;
	protected enum Direction {N,S,E,W}
	

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX=planetX;
		this.planetY=planetY;
		this.planetObstacles=planetObstacles;
		this.obstacles= new HashSet<>();   //the set of obstacles that the rover meets
		this.roverX=0;
		this.roverY=0;
		this.direction = Direction.N;
		this.status="(0,0,N)";
		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		boolean flag = false;
		int i;
		for (i=0; i<this.planetObstacles.size(); i++) {
			String planetObstacle = this.planetObstacles.get(i);
			String coordinates = planetObstacle.substring(1, planetObstacle.length()-1);
			String[] coord = coordinates.split(",");
			int coordX = Integer.parseInt(coord[0]);
			int coordY = Integer.parseInt(coord[1]);
			
			if(coordX==x && coordY==y) {
				flag = true;
			}
		}
		return flag;
	}
	//set the status
	public String setStatus() {
		this.status = "("+roverX + "," + roverY + ","+direction+")";
		
		StringBuilder sb = new StringBuilder(100);
		sb.append(status);
		for(String s : obstacles) {
			sb.append(s);
			
		}
		
		
		return sb.toString();
	}
	
	//forward and north
	public void commandFdirectionN() throws MarsRoverException{
		if(roverY==planetY-1) {
			if(!planetContainsObstacleAt(roverX,0)) {
				this.roverY=0;
				
			}
			else obstacles.add("("+roverX+","+0+")");
		}else if(!planetContainsObstacleAt(roverX,roverY+1)) { 
				this.roverY++;
				
		}
			else {
				int y=roverY+1;
				obstacles.add("("+roverX+","+y+")");
			}
		
	}
	//forward and east
	public void commandFdirectionE() throws MarsRoverException {
		if(roverX==planetX-1) {
			if(!planetContainsObstacleAt(0,roverY)) {
				this.roverX=0;
				
			}
			else obstacles.add("("+0+","+roverY+")");
		}else if(!planetContainsObstacleAt(roverX+1,roverY)) { 
				this.roverX++;
				
		}
			else {
				int x=roverX+1;
				String s = "("+x+","+roverY+")";
				obstacles.add(s);
			}
	}
	//forward and south
	public void commandFdirectionS() throws MarsRoverException {
		if(roverY==0) {
			if(!planetContainsObstacleAt(roverX,planetY-1)) {
				this.roverY=planetY-1;
				
			}
			else {
				int y=planetY-1;
				obstacles.add("("+roverX+","+y+")");
			}
		}else if(!planetContainsObstacleAt(roverX,roverY-1)) {
				this.roverY--;
				
		}
			else {
				int y=roverY-1;
				obstacles.add("("+roverX+","+y+")");
			}
	}
	//forward and west
	public void commandFdirectionW() throws MarsRoverException {
		if(roverX==0) {
			if(!planetContainsObstacleAt(planetX-1,roverY)) {
				this.roverX=planetX-1;
				
			}
			else {
				int x=planetX-1;
				obstacles.add("("+x+","+roverY+")");
			}
		}else if(!planetContainsObstacleAt(roverX-1,roverY)) {
				this.roverX--;
				
		}
			else {
				int x=roverX-1;
				obstacles.add("("+x+","+roverY+")");
			}
	}
	//the rover moves forward
	public void commandF() throws MarsRoverException{
		if(this.direction== Direction.N) {
			commandFdirectionN();
		}
		if(this.direction== Direction.E) {
			commandFdirectionE();
		}
		if(this.direction== Direction.S) {
			commandFdirectionS();
		}
		if(this.direction== Direction.W) {
			commandFdirectionW();
		}
	}
	//backward and north
	public void commandBdirectionN() throws MarsRoverException {
		if(roverY==0) {
			if(!planetContainsObstacleAt(roverX,planetY-1)) {
				this.roverY=planetY-1;
				
			}
			else {
				int y=planetY-1;
				obstacles.add("("+roverX+","+y+")");
			}
		}else if(!planetContainsObstacleAt(roverX,roverY-1)) {
			this.roverY--;
			
		}
			else {
				int y=roverY-1;
				obstacles.add("("+roverX+","+y+")");
			}
	}
	//backward and east
	public void commandBdirectionE() throws MarsRoverException {
		if(roverX==0) {
			if(!planetContainsObstacleAt(planetX-1,roverY)) {
				this.roverX=planetX-1;
				
			}
			else {
				int x=planetX-1;
				obstacles.add("("+x+","+roverY+")");
			}
		}else if(!planetContainsObstacleAt(roverX-1,roverY)) {
				this.roverX--;
				
		}
			else {
				int x=roverX-1;
				obstacles.add("("+x+","+roverY+")");
			}
	}
	//backward and south
	public void commandBdirectionS() throws MarsRoverException {
		if(roverY==planetY-1) {
			if(!planetContainsObstacleAt(roverX,0)) {
				roverY=0;
				
			}
				
			else{
				int y=0;
				obstacles.add("("+roverX+","+y+")");
			}
		}else if(!planetContainsObstacleAt(roverX,roverY+1)) {
				this.roverY++;
				
		}
			else {
				int y=roverY+1;
				obstacles.add("("+roverX+","+y+")");
			}
	}
	//backward and west
	public void commandBdirectionW() throws MarsRoverException {
		if(roverX==planetX-1) {
			if(!planetContainsObstacleAt(0,roverY)) {
				roverX=0;
				
			}
			else {
				int x=0;
				obstacles.add("("+x+","+roverY+")");
			}
		}else if(!planetContainsObstacleAt(roverX+1,roverY)) {
					this.roverX++;
					
			}
				else {
					int x=roverX+1;
					obstacles.add("("+x+","+roverY+")");
				}
	}
	//the rover moves backward
	public void commandB() throws MarsRoverException {
		if(this.direction== Direction.N) {
			commandBdirectionN();
		}
		if(this.direction== Direction.E) {
			commandBdirectionE();
		}
		if(this.direction== Direction.S) {
			commandBdirectionS();
		}
		if(this.direction== Direction.W) {
			commandBdirectionW();
		}
	}
	
	public void commandL() {
		Map<Integer,Direction> coordsR = new HashMap<>();
		coordsR.put(1, Direction.W);
		coordsR.put(2, Direction.S);
		coordsR.put(3, Direction.E);
		coordsR.put(4, Direction.N);
		int index=0;
		Set<Integer> dir = coordsR.keySet();
		for(int d : dir) {
			if(coordsR.get(d) == this.direction)
				index = d;
		}
		
		this.direction= coordsR.get(index%4 +1);
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		if(commandString.equals("")) {
			return status;
		}
		
		
		else if(commandString.equals("r")) {
			Map<Integer,Direction> coordsR = new HashMap<>();
			coordsR.put(1, Direction.N);
			coordsR.put(2, Direction.E);
			coordsR.put(3, Direction.S);
			coordsR.put(4, Direction.W);
			int index=0;
			Set<Integer> dir = coordsR.keySet();
			for(int d : dir) {
				if(coordsR.get(d) == this.direction)
					index = d;
			}
			
			this.direction= coordsR.get(index%4 +1);
			
			
		}
		else if(commandString.equals("l")) {
			commandL();
			
		}
		
		else if(commandString.equals("f")) {
			commandF();
			
		}
		
		else if(commandString.equals("b")) {
			commandB();
			
		}
		
		else if(Pattern.compile("\\w*",Pattern.CASE_INSENSITIVE).matcher(commandString).find()) {
					for(char c : commandString.toCharArray()) {
						executeCommand(Character.toString(c));
					}
		}
		
		
		this.status = setStatus();
		return status;
	}

}
